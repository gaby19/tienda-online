<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ',JE}Su0a>}gTcvUoS*9E$Xzk|.gyG3m-IF,`V>MiT+KP-YG,BjIbM5sh9vsF~,mV' );
define( 'SECURE_AUTH_KEY',  'FZ*c>&Igt(1`N+q6h De/4b#<z3fNNfM_G@,E0cea613|,^CEXAFCvXE#+aS;_/8' );
define( 'LOGGED_IN_KEY',    '6Q#X#4Sp$TyvFjJnXJPn|*=? $YNn~+Rl(-Fo<iM~o1CL;{}T@&q;geBDcAKTYo)' );
define( 'NONCE_KEY',        'f~E;c*pU;<Fcr;-IbE1x%6&j(;Zggpr:%Ki41t=!l~bGxr{.;;z&aJrlhv*ir5_%' );
define( 'AUTH_SALT',        'Wdo,Z0TbcKz8cyt$Z{ZXguvJ0:LTUqpJK!%bW|aqDC1S*EdEKR-v+F6lnO$n?,(9' );
define( 'SECURE_AUTH_SALT', ',Qo~D-NPtUZ@T[y~1l&U,9e8zK6pT%aXDh{#X3-_K|7MM?#N0E(|z47T^)}@`f^M' );
define( 'LOGGED_IN_SALT',   'CDjE82-jJf-x,^#%,s[6I<dT-PSZg1q7Jv5*[h#d[Eu@%QO{>U$S;g:xj&DTVP r' );
define( 'NONCE_SALT',       'f<kiXRTY|4=!58EmtqgxpOH*7pJ!8DcqlP,v^Fn2*zaYUlr2JqAcQ`Q ^_)zjz&Q' );
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

/*** FTP login ***/
define("FTP_HOST", "host-ftp");
define("FTP_USER", "nombre-usuario-ftp");
define("FTP_PASS", "password-ftp");
/*** Definir FS_METHOD en WordPress para actualizar de manera automatica sin FTP ***/
define("FS_METHOD","direct");


